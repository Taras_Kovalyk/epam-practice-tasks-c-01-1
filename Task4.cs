using System;

class Matrix
{
    int _rows;
    int _cols;
    int[,] _matrix;

    Matrix(int rows, int cols)
    {
        _rows = rows;
        _cols = cols;
        _matrix = new int[_rows, _cols];
    }

    public static Matrix operator +(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            var tempMatrix = new Matrix(m1._rows, m1._cols);

            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    tempMatrix._matrix[i, j] = m1._matrix[i, j] + m2._matrix[i, j];
                }
            }

            return tempMatrix;
        }
        else
        {
            Console.WriteLine("matrices are of different size");
            return null;
        }
    }

    public static Matrix operator -(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            var tempMatrix = new Matrix(m1._rows, m1._cols);

            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    tempMatrix._matrix[i, j] = m1._matrix[i, j] - m2._matrix[i, j];
                }
            }

            return tempMatrix;
        }
        else
        {
            Console.WriteLine("matrices are of different size");
            return null;
        }
    }

    public static Matrix operator *(Matrix m1, Matrix m2)
    {
        if (m1._cols == m2._rows)
        {
            var tempMatrix = new Matrix(m1._rows, m2._cols);
            for (int i = 0; i < tempMatrix._rows; i++)
            {
                for (int j = 0; j < tempMatrix._cols; j++)
                {
                    tempMatrix._matrix[i, j] = 0;
                    for (int k = 0; k < m1._cols; k++)
                    {
                        tempMatrix._matrix[i, j] = tempMatrix._matrix[i, j] + m1._matrix[i, k] * m2._matrix[k, j];
                    }
                }
            }

            return tempMatrix;
        }
        else
        {
            Console.WriteLine("matrices are not eligible for multiplying");
            return null;
        }
    }

    public static bool operator ==(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            bool flag = true;
            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    if (m1._matrix[i, j] != m2._matrix[i, j])
                    {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Console.WriteLine("Unnable to compare matrices");
            return false;
        }
    }

    public static bool operator !=(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            bool flag = false;
            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    if (m1._matrix[i, j] == m2._matrix[i, j])
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Console.WriteLine("Unnable to compare matrices");
            return false;
        }
    }

    public static bool operator >(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            bool flag = true;
            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    if (m1._matrix[i, j] < m2._matrix[i, j] || m1._matrix[i, j] == m2._matrix[i, j])
                    {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Console.WriteLine("Unnable to compare matrices");
            return false;
        }
    }

    public static bool operator <(Matrix m1, Matrix m2)
    {
        if (m1._rows == m2._rows && m1._cols == m2._cols)
        {
            bool flag = true;
            for (int i = 0; i < m1._rows; i++)
            {
                for (int j = 0; j < m1._cols; j++)
                {
                    if (m1._matrix[i, j] > m2._matrix[i, j] || m1._matrix[i, j] == m2._matrix[i, j])
                    {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Console.WriteLine("Unnable to compare matrices");
            return false;
        }
    }

    public Matrix GetTranspMatrix()
    {
        var tempMatrix = new Matrix(_cols, _rows);

        for (int i = 0; i < _rows; i++)
        {
            for (int j = 0; j < _cols; j++)
            {
                tempMatrix._matrix[j, i] = _matrix[i, j];
            }
        }

        return tempMatrix;
    }

    public void MultiplyByNumber(int number)
    {
        for (int i = 0; i < _rows; i++)
        {
            for (int j = 0; j < _cols; j++)
            {
                _matrix[i, j] *= number;
            }
        }
    }

	//in odrer to use this, x and y shows starting index of element in basic matrix, offsets show size of submatrix - starting element
	//for exaple basic matrix is 3,3. and you want to create submatrix with elements 1,1 1,2 2,1 2,2. Parametrs will be 1,1 for starting index and
	//1 and 1 for adding 1 more row and 1 more column to starting index.
    public Matrix GetSubMatrix(int x, int y, int rowsOffset, int colsOffset)
    {
        if (x + rowsOffset <= _rows && y + colsOffset < _cols)
        {
            var tempMatrix = new Matrix(rowsOffset + 1, colsOffset + 1);
            for (int i = 0; i < tempMatrix._rows; i++)
            {
                for (int j = 0; j < tempMatrix._cols; j++)
                {
                    tempMatrix._matrix[i, j] = _matrix[i + rowsOffset, j + colsOffset];
                }
            }

            return tempMatrix;
        }
        else
        {
            Console.WriteLine("Wrong size of submatrix");
            return null;
        }
    }

    public void PrintMatrix()
    {
        for (int i = 0; i < _rows; i++)
        {
            for (int j = 0; j < _cols; j++)
            {
                Console.Write("{0} ", _matrix[i, j]);
            }

            Console.Write("\n");
        }
    }

    static void Main()
    {
        var matr1 = new Matrix(3, 3);
        var matr2 = new Matrix(2, 3);
        var matr3 = new Matrix(3, 3);
		var matr4 = new Matrix(3, 3);

        for (int i = 0; i < matr1._rows; i++)
        {
            for (int j = 0; j < matr1._cols; j++)
            {
                matr1._matrix[i, j] = i + j;
            }
        }

        for (int i = 0; i < matr2._rows; i++)
        {
            for (int j = 0; j < matr2._cols; j++)
            {
                matr2._matrix[i, j] = i - j;
            }
        }

        for (int i = 0; i < matr3._rows; i++)
        {
            for (int j = 0; j < matr3._cols; j++)
            {
                matr3._matrix[i, j] = i * j;
            }
        }
		
		for (int i = 0; i < matr4._rows; i++)
        {
            for (int j = 0; j < matr4._cols; j++)
            {
                matr4._matrix[i, j] = 9;
            }
        }

        Console.WriteLine("Matrix 1");
        matr1.PrintMatrix();

        Console.WriteLine("Matrix 2");
        matr2.PrintMatrix();

        Console.WriteLine("Matrix 3");
        matr3.PrintMatrix();
		
		Console.WriteLine("Matrix 4");
		matr4.PrintMatrix();

        Console.WriteLine("Sum result of matr1 and matr3");
        var sumTest = matr1 + matr3;
        sumTest.PrintMatrix();

        Console.WriteLine("Sub result of matr3 and matr1");
        var subTest = matr3 - matr1;
        subTest.PrintMatrix();

        Console.WriteLine("Mult result of matr2 and matr3");
        var multTest = matr2 * matr3;
        multTest.PrintMatrix();

        Console.WriteLine("Transp matrix of matr2");
        var transTest = matr2.GetTranspMatrix();
        transTest.PrintMatrix();

        Console.WriteLine("Mult by number (2) matr2");
        matr2.MultiplyByNumber(2);
        matr2.PrintMatrix();

        Console.WriteLine("Get sub matrix of matr1");
        var subMatTest = matr1.GetSubMatrix(1, 1, 1, 1);
        subMatTest.PrintMatrix();
		
		if(matr1 == matr1)
		{
			Console.WriteLine("matr1 is eqal to matr1");
		}
		
		if(matr1 < matr2)
		{
			Console.WriteLine("matr2 is bigger than matr1");
		}
		
		if(matr1 < matr4)
		{
			Console.WriteLine("matr4 is bigger than matr1");
		}
		
		if(matr1 != matr3)
		{
			Console.WriteLine("matr1 is not eqal to matr2");
		}

        Console.ReadLine();
    }
}